package com.example.myapplication;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.BitSet;

class LinearAdapter extends RecyclerView.Adapter<LinearAdapter.myViewHolder> {

    private OnItemClickListener onItemClickListener;
    private Context context;
    private ArrayList<GoodsEntity> goodsEntities;

    public LinearAdapter(Context context,ArrayList<GoodsEntity> goodsEntities){
        this.context = context;
        this.goodsEntities = goodsEntities;
    }

    @NonNull
    @Override
    public LinearAdapter.myViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = View.inflate(context, R.layout.layout_linear_item,null);
        return new myViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull LinearAdapter.myViewHolder holder, int position) {
        GoodsEntity data = goodsEntities.get(position);
        holder.mItemGoodsName.setText(data.goodsName);
        holder.mItemGoodsPrice.setText(data.goodsPrice);
        holder.mItemGoodsNameTitle.setText(data.goodsNameTitle);
        holder.mItemGoodsPriceTitle.setText(data.goodsPriceTitle);
    }

    @Override
    public int getItemCount() {
        return goodsEntities.size();
    }

    class myViewHolder extends RecyclerView.ViewHolder {

        private ImageView mItemGoodsImg;
        private TextView mItemGoodsName;
        private TextView mItemGoodsPrice;
        private TextView mItemGoodsNameTitle;
        private TextView mItemGoodsPriceTitle;

        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            mItemGoodsImg = itemView.findViewById(R.id.item_goods_img);
            mItemGoodsName = itemView.findViewById(R.id.item_goods_name);
            mItemGoodsPrice = itemView.findViewById(R.id.item_goods_price);
            mItemGoodsNameTitle = itemView.findViewById(R.id.item_goods_nametitle);
            mItemGoodsPriceTitle = itemView.findViewById(R.id.item_goods_pricetitle);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (onItemClickListener!=null){

                        onItemClickListener.OnItemClick(v,goodsEntities.get(getLayoutPosition()));
                    }
                }
            });

        }
    }

    public interface OnItemClickListener {
        public void OnItemClick(View view, GoodsEntity data);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
