package com.example.myapplication;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class weixinFragment extends Fragment {

    private RecyclerView mRvMain; //定义ReclerView控件
    private View view;//定义view来设置fragment中的layout
    private ArrayList<GoodsEntity> goodsEntities = new ArrayList<GoodsEntity>();
    private LinearAdapter mLinearAdapter;


    public weixinFragment() {

        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.tab01, container, false);

        initRecyclerView();

        initData();

        return view;

    }

    //
    private void initRecyclerView() {
        mRvMain = (RecyclerView)view.findViewById(R.id.rv_main);
        mLinearAdapter = new LinearAdapter(getActivity(),goodsEntities);
        mRvMain.setAdapter(mLinearAdapter);
        mRvMain.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));

        mRvMain.addItemDecoration(new DividerItemDecoration(getActivity(),DividerItemDecoration.VERTICAL));

        mLinearAdapter.setOnItemClickListener(new LinearAdapter.OnItemClickListener() {
            @Override
            public void OnItemClick(View view, GoodsEntity data) {
                Toast.makeText(getActivity(),"图片售罄",Toast.LENGTH_SHORT).show();
            }
        });




    }

    //
    private void initData(){
        for (int i=0;i<10;i++){
            GoodsEntity goodsEntity = new GoodsEntity();
            goodsEntity.setGoodsNameTitle("  图片名称：");
            goodsEntity.setGoodsName("图片序号"+i);
            goodsEntity.setGoodsPriceTitle("  图片价格：");
            goodsEntity.setGoodsPrice("1"+i*100+"RMB");
            goodsEntity.setImgPath("../mipmap/ic_lancher");
            goodsEntities.add(goodsEntity);

        }
    }


}
