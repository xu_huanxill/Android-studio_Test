package com.example.myapplication;

import java.io.Serializable;

class GoodsEntity implements Serializable {
    public String imgPath;//图片地址
    public String goodsName;//货物名称
    public String goodsPrice;//货物价格
    public String goodsNameTitle;//商品名称标签
    public String goodsPriceTitle;//商品价格标签

    public GoodsEntity() {
    }

    public GoodsEntity(String imgPath, String goodsName, String goodsPrice,String goodsNameTitle,String goodsPriceTitle) {
        this.imgPath = imgPath;
        this.goodsName = goodsName;
        this.goodsPrice = goodsPrice;
        this.goodsNameTitle = goodsNameTitle;
        this.goodsPriceTitle = goodsPriceTitle;

    }

    //图片的路径获取方法
    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    //商品名字的获取方法
    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    //商品名字标签的获取方法
    public String getGoodsNameTitle(){
        return goodsNameTitle;
    }

    public void setGoodsNameTitle(String goodsNameTitle){
        this.goodsNameTitle = goodsNameTitle;
    }

    //商品价格标签的获取方法
    public  String getGoodsPriceTitle(){
        return goodsPriceTitle;
    }

    public void setGoodsPriceTitle(String goodsPriceTitle){
        this.goodsPriceTitle = goodsPriceTitle;
    }

    //商品价格的获取方法
    public String getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(String goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    @Override
    public String toString() {
        return "GoodsEntity{" +
                "imgPath='" + imgPath + '\'' +
                ", goodsName='" + goodsName + '\'' +
                ", goodsNameTitle='" + goodsNameTitle + '\'' +
                ", goodsPrice='" + goodsPrice + '\'' +
                ", goodsPriceTitle='" + goodsPriceTitle + '\'' +
                '}';
    }

}
